$("#keyword").keyup( function(){
    var ketikan = $("#keyword").val();
    console.log(ketikan);
    $.ajax({
        url : 'data?q='+ketikan,
        success : function(data){
            var array_items = data.items;
            console.log(array_items);
            $("#book_list").empty();
            if(array_items){
                if(array_items.length > 0){
                    $(".row-nothing").hide();
                    $(".thead-dark").empty();
                    $("#table-head").append("<tr><th>No</th><th>Cover</th><th>Information</th></tr>");
                }
                for(i = 0; i<array_items.length; i++){
                    var title = array_items[i].volumeInfo.title;
                    var author = array_items[i].volumeInfo.authors;
                    var page = array_items[i].volumeInfo.pageCount;
                    var published = array_items[i].volumeInfo.publishedDate;
                    var preview = array_items[i].volumeInfo.previewLink;




                    try{
                        var cover = array_items[i].volumeInfo.imageLinks.smallThumbnail;
                        var preview = array_items[i].volumeInfo.previewLink;
                        if(preview){
                            $("#book_list").append("<tr><td>"+ (i+1) +"</td><td><img src=" 
                        + cover +"></td><td><b>" 
                        +title+ "</b><br><br>Authored by "
                        +author+"<br>Published "
                        +published+"<br>"
                        +page+" pages<br><br><a href=" + preview+" target='_blank'><button class='btn btn-outline-danger back-btn' type='submit'>Preview</button></a></td></tr>")
                        }else{
                            $("#book_list").append("<tr><td>"+ (i+1) +"</td><td><img src=" 
                        + cover +"></td><td><b>" 
                        +title+ "</b><br><br>Authored by "
                        +author+"<br>Published "
                        +published+"<br>"
                        +page+" pages<br><br><a href=" + preview+"><button class='btn btn-outline-danger back-btn' type='submit'>Preview</button></a></td></tr>")
                        }
                        // $("#book_list").append("<tr><td>"+ (i+1) +"</td><td><img src=" 
                        // + cover +"></td><td><b>" 
                        // +title+ "</b><br><br>Authored by "
                        // +author+"<br>Published "
                        // +published+"<br>"
                        // +page+" pages<br><br><a href=" + preview+" target='_blank'><button class='btn btn-outline-danger back-btn' type='submit'>Preview</button></a></td></tr>")
                    }
                    catch(err){
                        $("#book_list").append("<tr><td>"+ (i+1) +"</td><td><img src='../../static/story8/noimage.png'></td><td><b>" 
                        +title+ "</b><br><br>Authored by "
                        +author+"<br>Published "
                        +published+"<br>"
                        +page+" pages</td></tr>")
                    }
                }
            }
            else{
                $("#table-head").empty();
                $(".row-nothing").show();
            }
        }
    });
});

