from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

class CreateUserForm(UserCreationForm):
    username = forms.CharField(label="")
    email = forms.CharField(label="")
    password1 = forms.CharField(widget=forms.PasswordInput(),label="")
    password2 = forms.CharField(widget=forms.PasswordInput(),label="")
    username.widget.attrs.update({'placeholder':'username', 'class':'input-box'})
    email.widget.attrs.update({'placeholder':'email', 'class':'input-box'})
    password1.widget.attrs.update({'placeholder':'enter password', 'class':'input-box'})
    password2.widget.attrs.update({'placeholder':'re-enter password', 'class':'input-box'})
    class Meta:
        model = User
        fields = ['username' , 'email', 'password1', 'password2']