from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .forms import CreateUserForm
from django.http import HttpResponse
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request,"trialapp/index.html")

def blank(request):
    return redirect("trialapp:index")

def signup(request):
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            messages.success(request,'registration success')

            return redirect('trialapp:login')
    else:
        form = CreateUserForm()
    return render(request,"trialapp/registration.html",{
        'form':form
    })

def loginPage(request):
	if request.user.is_authenticated:
		return redirect('trialapp:index')
	else:
		if request.method == 'POST':
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				login(request, user)
				return redirect('trialapp:index')
			else:
				messages.info(request, 'username or password is incorrect')

		context = {}
		return render(request, 'registration/login.html', context)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" +request.GET['q']
    ret = requests.get(url)
    data =json.loads(ret.content)

    if not request.user.is_authenticated:
        i=0
        for x in data['items']:
            data['items'][i]['volumeInfo']['previewLink'] = ''
            i+=1
    return JsonResponse(data, safe =False)