from django.contrib import admin
from django.urls import path, include
from . import views

app_name='trialapp'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('signup/', views.signup, name="signup"),
    path('accounts/login/', views.loginPage, name="loginPage"),
    path('accounts/', include('django.contrib.auth.urls')),
    path("target='_blank'", views.blank, name="blank"),
    path('data/', views.fungsi_data, name="data"),
]