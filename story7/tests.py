from django.test import TestCase, Client
from django.urls import reverse, resolve
from .views import *
from .urls import *

class Story7TestCase(TestCase):
    def test_is_home_page_exists(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7/index.html')

    def test_home_page_url_exist(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)

    def test_views_home(self):
        found = resolve('/story7/')            
        self.assertEqual(found.func, index)

    def test_back_button(self):
        response = Client().get('/story7/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("back", html_kembalian)

    def test_accordion_exist(self):
        response = Client().get('/story7/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Aktivitas", html_kembalian)
        self.assertIn("Kepanitiaan", html_kembalian)
        self.assertIn("Organisasi", html_kembalian)