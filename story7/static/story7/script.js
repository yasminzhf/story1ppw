jQuery(document).ready(function($){
    $('.desc').hide();
    $('dt p').click(function() {
        var $this = $(this);
        $this.parent().next().slideToggle();
      
        return false;
    });
    $(function() {
        $('.up').on('click', function() {
            var wrapper = $(this).closest('.item');
            wrapper.insertBefore(wrapper.prev());
        })
        $('.down').on('click', function() {
            var wrapper = $(this).closest('.item');
            wrapper.insertAfter(wrapper.next());
        });
    });
});