from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from .forms import MatkulForm
from .models import Matkul

# Create your views here.
def index(request):
    return render(request, "story5/index.html")

def detail(request,pk):
    matkul = Matkul.objects.get(pk=pk)
    return render(request, "story5/detail.html",{
        'matkul': matkul
    })

def matkul_list(request):
    matkul=Matkul.objects.all()
    return render(request,"story5/matkul_list.html",{
        'matkul':matkul
    })

def matkul_add(request):
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story5:matkul_list')
    else:
        form = MatkulForm()
    return render(request, "story5/matkul_add.html",{
        'form' : form
    })

def delete_matkul(request, pk):
    matkul = Matkul.objects.get(pk=pk)
    matkul.delete()
    return redirect('story5:matkul_list')

