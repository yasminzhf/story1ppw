from django.db import models

# Create your models here.
class Matkul(models.Model):
    name = models.CharField(max_length =30)
    dosen = models.CharField(max_length =30)
    total_credit = models.CharField(max_length =30)
    classroom = models.CharField(max_length =30)
    description= models.TextField(max_length =300)

    def __str__(self):
        return self.name