from django.contrib import admin
from django.urls import path, include
from . import views

app_name='story5'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('matkul/<int:pk>/', views.detail, name="detail"),
    path('matkul/<int:pk>/delete', views.delete_matkul, name='delete'),
    path('matkul/', views.matkul_list, name="matkul_list"),
    path('matkul/addmatkul/', views.matkul_add, name="matkul_add"),

]