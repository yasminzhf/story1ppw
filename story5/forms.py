from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields =('name','dosen','total_credit','classroom','description')