from django.test import TestCase, Client
from django.urls import reverse, resolve

# Create your tests here.
from .views import *
from .urls import *

class Story8TestCase(TestCase):
    def test_is_home_page_exists(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_home_page_url_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_views_home(self):
        found = resolve('/story8/')            
        self.assertEqual(found.func, index)

    def test_views_api(self):
        response = Client().get(reverse('story8:data')+"?q=test")
        self.assertIn("items", response.content.decode('utf-8'))
