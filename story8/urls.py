from django.contrib import admin
from django.urls import path, include
from . import views

app_name='story8'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('data/', views.fungsi_data, name="data"),
]