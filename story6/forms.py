from django import forms
from .models import Event,Attendance

class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields =('event_name','description')

class AttendanceForm(forms.ModelForm):
    class Meta:
        model = Attendance
        fields =('name',)