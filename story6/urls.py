from django.contrib import admin
from django.urls import path, include
from . import views

app_name='story6'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name="index"),
    path('tambah/', views.tambah_event, name="tambah_event"),
    path('daftar/<int:id>', views.tambah_attendance, name="tambah_attendance"),
]