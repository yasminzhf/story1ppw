from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Event, Attendance
from .views import *
from .urls import *

class Story6TestCase(TestCase):

# Test untuk Views
    # Test Function
    def test_views_home(self):
        found = resolve('/story6/')            
        self.assertEqual(found.func, index)

    def test_views_tambah_event(self):
        found = resolve('/story6/tambah/')            
        self.assertEqual(found.func, tambah_event)

    def test_views_tambah_peserta(self):
        Event.objects.create(event_name='PPW')
        found = resolve('/story6/daftar/1')            
        self.assertEqual(found.func, tambah_attendance)
    
    # Test template used
    def test_is_home_page_exists(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')

    def test_is_tambah_event_page_exists(self):
        response = Client().get('/story6/tambah/')
        self.assertTemplateUsed(response, 'story6/tambah_event.html')


# Module Test
    
    # Module Functionality Test
    def test_check_model_create_event(self):
        Event.objects.create(event_name="PPW")
        amount = Event.objects.all().count()
        self.assertEqual(amount, 1)

    def test_check_model_create_anggota(self):
        Event.objects.create(event_name="PPW")
        obj1 = Event.objects.get(id=1)
        
        Attendance.objects.create(event=obj1, name="Nama Orang")
        amount = Attendance.objects.all().count()
        self.assertEqual(amount, 1)

    # Module Return Test
    def test_check_model_return_event(self):
        Event.objects.create(event_name="PPW")
        result = Event.objects.get(id=1)
        self.assertEqual(str(result), "PPW")

    def test_check_model_return_anggota(self):
        Event.objects.create(event_name="PPW")
        obj1 = Event.objects.get(id=1)
        
        Attendance.objects.create(event=obj1, name="Nama Orang")
        result = Attendance.objects.get(id=1)
        self.assertEqual(str(result), "Nama Orang")


#  Url Test
    
    #Get
    def test_home_create_url_exists(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_kegiatan_create_url_exists(self):
        response = Client().get('/story6/tambah/')
        self.assertEqual(response.status_code, 200)

    #Post
    def test_tambah_kegiatan_post_url(self):
        response = Client().post('/story6/tambah/', data={'event_name': 'Belajar',})
        amount = Event.objects.filter(event_name="Belajar").count()
        self.assertEqual(amount, 1)

    def test_tambah_peserta_post_url(self):
        Event.objects.create(event_name='PPW')
        response = Client().post('/story6/daftar/1', data={'name': 'Nama Orang',})
        amount = Attendance.objects.filter(name="Nama Orang").count()
        self.assertEqual(amount, 1)



