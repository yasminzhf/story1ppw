from django.shortcuts import render, redirect
from .models import Event,Attendance
from .forms import EventForm, AttendanceForm

# Create your views here.
def index(request):
    event = Event.objects.all()
    attendance = Attendance.objects.all()
    return render(request, "story6/index.html",{
        'event':event,
        'attendance':attendance,
    })

def tambah_event(request):
    if request.method == 'POST':
        form1 = EventForm(request.POST)
        if form1.is_valid():
            form1.save()
            return redirect('story6:index')
    else:
        form1 = EventForm()
    return render(request, "story6/tambah_event.html",{
        'form1' : form1
    })

def tambah_attendance(request, id):
    if request.method == 'POST':
        form2 = AttendanceForm(request.POST)
        if form2.is_valid():
            validated_form = form2.save(commit=False)
            validated_form.event_id = Event.objects.get(pk=id).id
            validated_form.save()
            return redirect('story6:index')
    else:
        form2 = AttendanceForm()
    return render(request, "story6/tambah_attendance.html",{
        'form2' : form2
    })
