from django.db import models

# Create your models here.
class Event(models.Model):
    event_name = models.CharField(max_length = 50)
    description = models.TextField(max_length = 300)

    def __str__(self):
        return self.event_name

class Attendance(models.Model):
    name = models.CharField(max_length=30)
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name ='attendances')

    def __str__(self):
        return self.name 